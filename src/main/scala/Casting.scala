import org.apache.spark.sql.types.{StructField, StructType}
import org.apache.spark.sql.{Encoders, Row, SparkSession}

import scala.io.StdIn

object Casting extends App {

  val spark =
    SparkSession.builder.appName("Casting").master("local[1]").getOrCreate()

  case class ComplexNumber(real: Double, imaginary: Double) {

    def plus(c: ComplexNumber): ComplexNumber = ComplexNumber(real + c.real, imaginary + c.imaginary)
  }

  import spark.implicits._

  val complexNumberSchema = Encoders.product[ComplexNumber].schema

  def c(real: Double, imaginary: Double): Row = {
    Row(real, imaginary)
  }

  val schema = StructType(
    Seq("a", "b").map { ch =>
      StructField(s"complex_$ch", complexNumberSchema, nullable = false)
    }
  )

  val row = Row(c(1, 2), c(2, 9))
  val df =
    spark.createDataFrame(spark.sparkContext.parallelize(Seq(row)), schema)

  val typedDf = df.select($"complex_a.*").as[ComplexNumber].map(_.plus(ComplexNumber(2, 2)))
  typedDf.explain()
  typedDf.printSchema()
  typedDf.show()

  StdIn.readLine("Press [return] to close the Spark Session.")
  spark.close()
}
