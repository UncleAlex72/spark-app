import org.apache.spark.sql._
import org.apache.spark.sql.catalyst.encoders.RowEncoder
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types.{IntegerType, StructField, StructType}

object Pipeline extends App {

  private val NUMBER = "number"
  private val INDEX = "index"

  case class DataFrameDecide(
      df: DataFrame,
      decisions: Seq[(Row => Boolean, DataFrame => DataFrame)] = Seq.empty
  ) {
    def when(
        predicate: Row => Boolean,
        transform: DataFrame => DataFrame
    ): DataFrameDecide = {
      copy(decisions = decisions :+ (predicate, transform))
    }

    def join(): DataFrame = {
      val dataFrames: Seq[DataFrame] = decisions.map {
        case (predicate, transformer) =>
          df.filter(predicate).transform(transformer)
      }
      dataFrames.reduce(_.union(_))
    }

    def otherwise(
        transformer: DataFrame => DataFrame
    )(implicit session: SparkSession): DataFrame = {
      val predicate =
        decisions
          .map(_._1)
          .reduce((p1, p2) => (row: Row) => !p1(row) && !p2(row))
      when(predicate, transformer).join()
    }
  }

  case class DataFrameFork(
      df: DataFrame,
      forks: Seq[(DataFrame => Column, DataFrame => DataFrame)] = Seq.empty
  ) {
    def when(
        column: DataFrame => Column,
        transform: DataFrame => DataFrame
    ): DataFrameFork = {
      copy(forks = forks :+ (column, transform))
    }

    def join(): DataFrame = {
      val dataFrames: Seq[DataFrame] = forks.map {
        case (columnFactory, transformer) =>
          df.where(columnFactory(df)).transform(transformer)
      }
      dataFrames.reduce(_.union(_))
    }

    def otherwise(
        transformer: DataFrame => DataFrame
    )(implicit session: SparkSession): DataFrame = {
      val column = (df: DataFrame) =>
        not(forks.map(fk => fk._1(df)).reduce(_.or(_)))
      when(column, transformer).join()
    }
  }

  implicit class DataFrameExtensions(df: DataFrame) extends Serializable {
    def decide(): DataFrameDecide = DataFrameDecide(df)

    def fork(): DataFrameFork = DataFrameFork(df)
  }

  implicit val session: SparkSession =
    SparkSession.builder().appName("flatmap").master("local[*]").getOrCreate()

  val schema: StructType = StructType(
    Seq(StructField(NUMBER, IntegerType), StructField(INDEX, IntegerType))
  )
  implicit val rowEncoder: Encoder[Row] = RowEncoder(schema)

  def plus(i: Int)(df: DataFrame): DataFrame = {
    df.select(col(NUMBER).plus(i).as(NUMBER), col(INDEX))
  }

  def times(i: Int)(df: DataFrame): DataFrame = {
    df.select(col(NUMBER).multiply(i).as(NUMBER), col(INDEX))
  }

  val rdd =
    session.sparkContext.makeRDD(Seq(1, 10, 17).zipWithIndex.map {
      case (n, idx) => Row(n, idx)
    })
  val df = session.createDataFrame(rdd, schema)

  val newDf = df
    .transform(plus(1))
    .decide()
    .when(
      (row: Row) => row.getAs[Int](NUMBER) == 11,
      df => df.transform(times(3))
    )
    .when((row: Row) => row.getAs[Int](NUMBER) < 11, df => df)
    .otherwise(df => df.transform(times(4)))

  newDf.explain()
  /*
  == Physical Plan ==
  Union
  :- *(1) Project [(number#8 * 3) AS number#11, index#5]
  :  +- *(1) Filter Pipeline$$$Lambda$1190/1653634548@44641d6c.apply
  :     +- *(1) Project [(number#4 + 1) AS number#8, index#5]
  :        +- *(1) Scan ExistingRDD[number#4,index#5]
  :- *(2) Filter Pipeline$$$Lambda$1192/1916224178@1ae924f1.apply
  :  +- *(2) Project [(number#4 + 1) AS number#8, index#5]
  :     +- *(2) Scan ExistingRDD[number#4,index#5]
  +- *(3) Project [(number#8 * 4) AS number#21, index#5]
     +- *(3) Filter Pipeline$DataFrameDecide$$Lambda$1197/1855297340@59d5a6fd.apply
        +- *(3) Project [(number#4 + 1) AS number#8, index#5]
           +- *(3) Scan ExistingRDD[number#4,index#5]
   */

  newDf.show()
  /*
  +------+-----+
  |number|index|
  +------+-----+
  |    33|    1|
  |     2|    0|
  |    72|    2|
  +------+-----+
   */

  val forkedDf = df
    .transform(plus(1))
    .fork()
    .when(_.col(NUMBER) === 11, df => df.transform(times(3)))
    .when(_.col(NUMBER) < 11, df => df)
    .otherwise(df => df.transform(times(4)))

  forkedDf.explain()
  /*
  == Physical Plan ==
  Union
  :- *(1) Project [((number#4 + 1) * 3) AS number#41, index#5]
  :  +- *(1) Filter (isnotnull(number#4) AND ((number#4 + 1) = 11))
  :     +- *(1) Scan ExistingRDD[number#4,index#5]
  :- *(2) Project [(number#4 + 1) AS number#47, index#5]
  :  +- *(2) Filter (isnotnull(number#4) AND ((number#4 + 1) < 11))
  :     +- *(2) Scan ExistingRDD[number#4,index#5]
  +- *(3) Project [((number#4 + 1) * 4) AS number#51, index#5]
     +- *(3) Filter ((isnotnull(number#4) AND NOT ((number#4 + 1) = 11)) AND ((number#4 + 1) >= 11))
        +- *(3) Scan ExistingRDD[number#4,index#5]
   */

  forkedDf.show()
  /*
  +------+-----+
  |number|index|
  +------+-----+
  |    33|    1|
  |     2|    0|
  |    72|    2|
  +------+-----+
   */
  session.close()
}
