import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._

object Pairs extends App {

  val spark =
    SparkSession.builder.appName("Pairs").master("local[1]").getOrCreate()

  import spark.implicits._

  val df = spark.sparkContext.parallelize(Range(0, 10)).toDF("v")
  val dfx = df.withColumnRenamed("v", "x")
  val dfy = df.withColumnRenamed("v", "y")

  val dfxy =
    dfx.crossJoin(dfy).select(struct(dfx("x"), dfy("y")).as("xy"))

  dfxy.printSchema()
  dfxy.explain()
  dfxy.show()
  spark.close()
}
