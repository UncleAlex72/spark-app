import org.apache.spark.SparkConf
import org.apache.spark.sql.{Encoder, Encoders, SaveMode, SparkSession}

object JsonWriterApp extends App {

  implicit val outputEncoder: Encoder[Output] = Encoders.product[Output]
  implicit val stringEncoder: Encoder[String] = Encoders.STRING

  val spark = SparkSession.
    builder.
    config(new SparkConf().registerKryoClasses(Array(Output.getClass))).
    appName("Simple Scala Application").
    master("local[1]").
    getOrCreate()

  spark.createDataset(Seq("Brian", "Freddie"))
    .map(Output)
    .coalesce(1)
    .write
    .mode(SaveMode.Overwrite)
    .json("output")
}

case class Output(name: String)