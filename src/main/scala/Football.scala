import org.apache.spark.sql.functions._
import org.apache.spark.sql.{DataFrame, SparkSession}
import org.apache.spark.sql.types.{StringType, StructField, StructType}

object Football extends App {

  val session =
    SparkSession.builder().master("local[*]").appName("football").getOrCreate()

  private val NAME = "name"
  private val COUNT = "count"

  val schema = StructType(Seq(StructField(NAME, StringType)))

  val teams: DataFrame =
    session.read.schema(schema).csv("src/main/resources/teams.csv")
  val matches: DataFrame =
    session.read.schema(schema).csv("src/main/resources/matches.csv")

  val counts: DataFrame =
    matches.select(col(NAME)).groupBy(col(NAME)).count()

  val allCounts: DataFrame =
    teams
      .join(counts, teams.col(NAME) === matches.col(NAME), "leftouter")
      .select(
        teams.col(NAME),
        when(col(COUNT).isNull, 0).otherwise(col(COUNT)).as("ppv")
      )
      .orderBy(col(COUNT).desc, col(NAME))

  allCounts.show()
  session.close()
}
