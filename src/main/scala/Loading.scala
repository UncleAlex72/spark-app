import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types.{
  IntegerType,
  StringType,
  StructField,
  StructType
}

import scala.util.{Failure, Success, Try}

object Loading extends App {

  val sparkSession =
    SparkSession.builder().appName("Loading").master("local[1]").getOrCreate()

  import sparkSession._

  val schema: StructType = StructType(
    Seq(
      StructField("letter", StringType, nullable = false),
      StructField("number", IntegerType, nullable = false)
    ))

  case class Whotsit(letter: String, number: Int)

  Seq(
    "correct",
    "wrong-header",
    "missing-column",
    "too-many-columns",
    "wrong-type"
  ).foreach { filename =>
    println(s"----$filename----")
    Try {
      sparkSession.read
        .option("header", value = true)
        .option("inferSchema", value = false)
        .format("csv")
        .load(s"src/main/resources/$filename.csv")
        .withColumn("number", col("number").cast(IntegerType))
        .withColumn("error", col("number").isNull.or(col("letter").isNull))
    } match {
      case Success(df) =>
        df.printSchema()
        df.show()
      case Failure(exception) =>
        println(exception.getMessage)
        exception.printStackTrace(System.out)
    }
  }
}
