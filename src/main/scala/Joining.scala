import org.apache.spark.sql.types.{IntegerType, StructField, StructType}
import org.apache.spark.sql.{Column, DataFrame, Row, SparkSession}

object Joining extends App {

  val spark =
    SparkSession.builder.appName("Joining").master("local[1]").getOrCreate()

  def df(name: String, multiplier: Int): (DataFrame, Column) = {
    val rdd = spark.sparkContext
      .parallelize(Range(0, 10))
      .map(_ * multiplier)
      .map(n => Row(Row(n)))
    val innerSchema = StructType(
      Seq(
        StructField(name, IntegerType, nullable = false)
      )
    )
    val schema = StructType(
      Seq(
        StructField(name, innerSchema, nullable = false)
      )
    )
    val dataFrame = spark.createDataFrame(rdd, schema)
    (dataFrame, dataFrame.col(s"$name.$name"))
  }

  val (threesDf, three) = df("three", 3)
  val (fivesDf, five) = df("five", 5)

  threesDf.printSchema()
  fivesDf.printSchema()

  val threesAndFivesDf =
    threesDf.join(fivesDf.as("five"), three === five, "left")
  val df: DataFrame =
    threesAndFivesDf.withColumn("match", five.isNotNull).sort(three)
  df.show()
  df.explain()

  spark.close()
}
