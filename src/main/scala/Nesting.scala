import org.apache.spark.sql.{Row, SparkSession}
import org.apache.spark.sql.types.{
  DoubleType,
  StringType,
  StructField,
  StructType
}
import org.apache.spark.sql.functions._

import scala.util.{Success, Try}
object Nesting extends App {

  val spark =
    SparkSession.builder.appName("Nesting").master("local[1]").getOrCreate()

  val complexNumberSchema = StructType(
    Seq(
      StructField("discriminator", StringType, nullable = false),
      StructField("real", DoubleType, nullable = false),
      StructField("imaginary", DoubleType, nullable = false)
    ))

  def c(real: Double, imaginary: Double): Row = {
    Row("complex", real, imaginary)
  }

  val schema = StructType(
    Seq(
      StructField("real_a", DoubleType, nullable = false),
      StructField("complex_a", complexNumberSchema, nullable = false),
      StructField("complex_b", complexNumberSchema, nullable = false),
      StructField("real_b", DoubleType, nullable = false)
    ))

  val row = Row(2.0, c(1.0, 2.0), c(3.0, 5.0), 7.0)
  val df =
    spark.createDataFrame(spark.sparkContext.parallelize(Seq(row)), schema)
  df.show()

  val reals = df.columns.foldLeft(df) { (acc, columnName) =>
    Try(
      acc
        .select(acc.col(s"$columnName.real").as(columnName))
        .where(acc.col(s"$columnName.discriminator") === lit("complex"))
    ) match {
      case Success(realDf) =>
        acc.drop(columnName).crossJoin(realDf)
      case _ => acc
    }
  }

  val imaginaries = df.columns.foldLeft(df) { (acc, columnName) =>
    Try(
      acc
        .select(acc.col(s"$columnName.imaginary").as(columnName))
        .where(acc.col(s"$columnName.discriminator") === lit("complex"))
    ) match {
      case Success(imaginaryDf) =>
        acc.drop(columnName).crossJoin(imaginaryDf)
      case _ => acc.withColumn(columnName, lit(0d))
    }
  }

  Seq("Reals" -> reals, "Imaginaries" -> imaginaries).foreach { case (name, df) =>
    println(name)
    df.explain()
    df.printSchema()
    df.show()
  }
}
