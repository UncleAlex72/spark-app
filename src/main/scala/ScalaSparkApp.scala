import java.time.LocalTime

import org.apache.spark.TaskContext
import org.apache.spark.sql.{Encoder, Encoders, SparkSession}
import org.slf4j.{Logger, LoggerFactory}

object ScalaSparkApp extends App {

  val logger: Logger = LoggerFactory.getLogger("app")

  implicit val longEncoder: Encoder[String] = Encoders.javaSerialization[String]

  val spark = SparkSession.
    builder.
    appName("Simple Scala Application").
    master("local[6]").
    getOrCreate()

  val sc = spark.sparkContext
  val input = sc.parallelize(0 to 100)

  val rdd = input.groupBy(_ / 10).flatMap { case (_, values) =>
    val tc = TaskContext.get()
    Thread.sleep(10)
    values.map(Obj(_, tc.partitionId(), LocalTime.now()))
  }

  val results = rdd.sortBy(_.when).collect()
  spark.stop()

  results.foreach { obj =>
    logger.info(s"${obj.partitionId}: ${obj.value}")
  }
}

case class Obj(value: Int, partitionId: Int, when: LocalTime)