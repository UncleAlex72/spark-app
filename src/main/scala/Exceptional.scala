import org.apache.spark.sql.functions._
import org.apache.spark.sql.{DataFrame, SparkSession}

import scala.io.StdIn
import scala.util.Try

object Exceptional extends App {

  val spark =
    SparkSession.builder.appName("Exceptional").master("local[2]").getOrCreate()

  import spark.implicits._

  def df(columnName: String, numbers: Seq[Int]): DataFrame = {
    spark.sparkContext.parallelize(numbers).toDF.withColumnRenamed("value", columnName)
  }

  val acc = spark.sparkContext.collectionAccumulator[Int]("numbers")

  val nonNull = udf { (left: Integer, right: Integer) =>
    if (right == null) {
      throw new IllegalStateException(s"So I told you! $left")
    } else {
      acc.add(right)
      right
    }
  }

  val bigDf = df("big", Range(0, 10))
  val smallDf = df("small", Range(0, 9))
  val joinDf = bigDf
    .join(smallDf, col("big") === col("small") , "left")
      .withColumn("small", nonNull(col("big"), col("small")))

  Try(joinDf.show())
  println(acc.value)

  spark.close()
}
