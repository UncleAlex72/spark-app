package pipeline;

import com.google.common.collect.Streams;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.apache.spark.sql.Column;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import scala.Function1;
import scala.runtime.AbstractFunction1;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.function.UnaryOperator;
import java.util.stream.Stream;

import static org.apache.spark.sql.functions.lit;
import static org.apache.spark.sql.functions.not;

@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public class PipelineWithDs {

  private final Dataset<Row> dataset;

  public static PipelineWithDs pipeline(Dataset<Row> dataset) {
      return new PipelineWithDs(dataset);
  }

    public PipelineWithDs andThen(UnaryOperator<Dataset<Row>> transform) {
        return new PipelineWithDs(dataset.transform(f(transform)));
    }

    public Dataset<Row> execute() {
      return dataset;
    }

    public Fork fork() {
      return new Fork();
    }

    @NoArgsConstructor(access = AccessLevel.PRIVATE)
    public class Fork {

      private final List<Function<Dataset<Row>, Column>> columns = new ArrayList<>();
        private final List<Function1<Dataset<Row>, Dataset<Row>>> transforms = new ArrayList<>();

      public Fork when(Function<Dataset<Row>, Column> column, UnaryOperator<Dataset<Row>> transform) {
          columns.add(column);
          transforms.add(f(transform));
          return this;
      }

      public PipelineWithDs otherwise(UnaryOperator<Dataset<Row>> transform) {
          Function<Dataset<Row>, Column> otherwiseColumn =
                  ds -> not(columns.stream().map(col -> col.apply(ds)).reduce(Column::or).orElse(lit(false)));
          Stream<Dataset<Row>> datasets = Streams.zip(Stream.concat(columns.stream(), Stream.of(otherwiseColumn)),
                  Stream.concat(transforms.stream(), Stream.of(f(transform))),
                  (thisColumn, thisTransform) -> dataset.where(thisColumn.apply(dataset)).transform(thisTransform));
          Dataset<Row> newDataset = datasets.reduce(Dataset::union).get();
          return new PipelineWithDs(newDataset);
      }
    }
  private static Function1<Dataset<Row>, Dataset<Row>> f(Function<Dataset<Row>, Dataset<Row>> fn) {
      return new AbstractFunction1<Dataset<Row>, Dataset<Row>>() {
          @Override
          public Dataset<Row> apply(Dataset<Row> ds) {
              return fn.apply(ds);
          }
      };
  }
}
