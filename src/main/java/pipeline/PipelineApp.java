package pipeline;

import com.google.common.collect.Streams;
import org.apache.spark.sql.*;
import org.apache.spark.sql.catalyst.encoders.RowEncoder;
import org.apache.spark.sql.types.Metadata;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;

import java.util.List;
import java.util.function.UnaryOperator;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.apache.spark.sql.functions.col;
import static org.apache.spark.sql.types.DataTypes.IntegerType;
import static pipeline.Pipeline.pipeline;
import static pipeline.Pipeline.when;

public class PipelineApp {

    private static final String NUMBER = "number";
    private static final String INDEX = "index";

    public static void main(String... args) {
        try (SparkSession session = SparkSession.builder().appName("pipeline").master("local[*]").getOrCreate()) {
            StructType schema = new StructType(new StructField[] {
                    new StructField(NUMBER, IntegerType, false, Metadata.empty()),
                    new StructField(INDEX, IntegerType, false, Metadata.empty())
            });
            Encoder<Row> encoder = RowEncoder.apply(schema);
            //1, 10, 17
            List<Row> rows = Streams.mapWithIndex(Stream.of(1, 10, 17),
                    (number, index) -> RowFactory.create(number, (int) index)).collect(Collectors.toList());
            Dataset<Row> ds = session.createDataset(rows, encoder).repartition(3);

            Pipeline p = pipeline(plus(1)).andThen(
                    when(df -> df.col(NUMBER).equalTo(11), times(3)).
                    when(df -> df.col(NUMBER).lt(11), df -> df)
                    .otherwise(times(4))
            ).andThen(plus(2));

            Dataset<Row> transformedDs = p.execute(ds);

            transformedDs.explain();
            transformedDs.show();
        }
    }

    public static UnaryOperator<Dataset<Row>> plus(int i) {
        return ds -> ds.select(col(NUMBER).plus(i).as(NUMBER), col(INDEX));
    }

    public static UnaryOperator<Dataset<Row>> times(int i) {
        return ds -> ds.select(col(NUMBER).multiply(i).as(NUMBER), col(INDEX));
    }
}
