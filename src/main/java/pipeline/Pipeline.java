package pipeline;

import com.google.common.collect.Streams;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import org.apache.spark.sql.Column;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.functions;
import scala.Function1;
import scala.runtime.AbstractFunction1;

import java.util.ArrayList;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.UnaryOperator;
import java.util.stream.Stream;

import static org.apache.spark.sql.functions.lit;

@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public class Pipeline {

  private final Function1<Dataset<Row>, Dataset<Row>> transform;

  public static Pipeline pipeline(UnaryOperator<Dataset<Row>> transform) {
      return new Pipeline(f(transform));
  }

  public static Pipeline compose(Pipeline first, Pipeline... others) {
    return Stream.of(others).reduce(first, Pipeline::andThen);
  }

    public static Fork when(Function<Dataset<Row>, Column> column, UnaryOperator<Dataset<Row>> transform) {
        return new Fork(column, transform);
    }

    public Pipeline andThen(UnaryOperator<Dataset<Row>> transform) {
        return new Pipeline(this.transform.andThen(f(transform)));
    }

    public Dataset<Row> execute(Dataset<Row> dataset) {
      return dataset.transform(this.transform);
    }

    public Pipeline andThen(Pipeline other) {
      return new Pipeline(this.transform.andThen(other.transform));
    }

    public static class Fork {

      private final List<Function<Dataset<Row>, Column>> columns = new ArrayList<>();
        private final List<Function1<Dataset<Row>, Dataset<Row>>> transforms = new ArrayList<>();

      private Fork(Function<Dataset<Row>, Column> column, UnaryOperator<Dataset<Row>> transform) {
          when(column, transform);
      }

      public Fork when(Function<Dataset<Row>, Column> column, UnaryOperator<Dataset<Row>> transform) {
          columns.add(column);
          transforms.add(f(transform));
          return this;
      }

      public UnaryOperator<Dataset<Row>> otherwise(UnaryOperator<Dataset<Row>> otherwiseTransform) {
          Function<Dataset<Row>, Column> otherwiseColumn = ds ->
                  columns.stream().map(c -> c.apply(ds)).reduce(Column::or).map(functions::not).orElse(lit(true));
          return ds -> {
              BiFunction<Function<Dataset<Row>, Column>, Function1<Dataset<Row>, Dataset<Row>>, Dataset<Row>> datasetBuilder =
                      (thisColumn, thisTransform) -> ds.where(thisColumn.apply(ds)).transform(thisTransform);
              Stream<Dataset<Row>> datasets = Streams.zip(columns.stream(), transforms.stream(),
                      datasetBuilder);
              return datasets.reduce(datasetBuilder.apply(otherwiseColumn, f(otherwiseTransform)), Dataset::union);
          };
      }
    }
  private static Function1<Dataset<Row>, Dataset<Row>> f(Function<Dataset<Row>, Dataset<Row>> fn) {
      return new AbstractFunction1<Dataset<Row>, Dataset<Row>>() {
          @Override
          public Dataset<Row> apply(Dataset<Row> ds) {
              return fn.apply(ds);
          }
      };
  }
}
