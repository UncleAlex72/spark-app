package monads;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.Value;

import java.util.function.Function;
import java.util.function.Supplier;

public interface EvalMonad<E> {

    <F> EvalMonad<F> map(Function<E, F> mapper);

    <F> EvalMonad<F> flatMap(Function<E, EvalMonad<F>> flatMapper);

    E get();

    static <E> EvalMonad<E> now(E value) {
        return new Now<E>(value);
    }

    static <E> EvalMonad<E> lazy(Supplier<E> valueSupplier) {
        return new Lazy<>(valueSupplier);
    }

    @SuppressWarnings("RedundantModifiersValueLombok")
    @Value
    @RequiredArgsConstructor(access = AccessLevel.PRIVATE)
    class Now<E> implements EvalMonad<E> {

        private final E value;

        @Override
        public <F> EvalMonad<F> map(Function<E, F> mapper) {
            return new Now<>(mapper.apply(value));
        }

        @Override
        public <F> EvalMonad<F> flatMap(Function<E, EvalMonad<F>> flatMapper) {
            return flatMapper.apply(value);
        }

        @Override
        public E get() {
            return value;
        }

    }

    @SuppressWarnings("RedundantModifiersValueLombok")
    @RequiredArgsConstructor(access = AccessLevel.PRIVATE)
    @Value
    class Lazy<E> implements EvalMonad<E> {

        private final Supplier<E> valueSupplier;

        @Override
        public <F> EvalMonad<F> map(Function<E, F> mapper) {
            Supplier<F> newSupplier = () -> mapper.apply(valueSupplier.get());
            return lazy(newSupplier);
        }

        @Override
        public <F> EvalMonad<F> flatMap(Function<E, EvalMonad<F>> flatMapper) {
            Supplier<F> newSupplier = () -> flatMapper.apply(valueSupplier.get()).get();
            return lazy(newSupplier);
        }

        @Override
        public E get() {
            System.out.println("Wheee!");
            return valueSupplier.get();
        }
    }
}
