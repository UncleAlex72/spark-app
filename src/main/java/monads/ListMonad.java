package monads;

import lombok.Value;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

@SuppressWarnings("RedundantModifiersValueLombok")
@Value
public class ListMonad<E> {

    private final List<E> values;

    public <F> ListMonad<F> map(Function<E, F> mapper) {
        List<F> newValues = new ArrayList<>();
        for (E value: values) {
            newValues.add(mapper.apply(value));
        }
        return new ListMonad<>(newValues);
    }

    public <F> ListMonad<F> flatMap(Function<E, ListMonad<F>> flatMapper) {
        List<F> newValues = new ArrayList<>();
        for (E value: values) {
            newValues.addAll(flatMapper.apply(value).getValues());
        }
        return new ListMonad<>(newValues);
    }

}
