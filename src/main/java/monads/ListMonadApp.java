package monads;

import lombok.Value;

import java.util.Arrays;
import java.util.List;

public class ListMonadApp {

  private static final List<Integer> NUMBERS = Arrays.asList(1, 2, 3);
  private static final ListMonad<Integer> MONAD = new ListMonad<>(NUMBERS);

  @SuppressWarnings("RedundantModifiersValueLombok")
  @Value
  static class ThreeDee {
    private final int x;
    private final int y;
    private final int z;
  }

  public static void main(String... args) {
    ListMonad<ThreeDee> threeDeeListMonad =
        MONAD.flatMap(x -> MONAD.flatMap(y -> MONAD.map(z -> new ThreeDee(x, y, z))));
    threeDeeListMonad.getValues().forEach(System.out::println);
  }
}
