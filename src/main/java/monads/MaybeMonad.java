package monads;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.Value;

import java.util.function.Function;

public interface MaybeMonad<E> {

    <F> MaybeMonad<F> map(Function<E, F> mapper);

    <F> MaybeMonad<F> flatMap(Function<E, MaybeMonad<F>> flatMapper);

    E get();

    boolean isDefined();

    static <E> MaybeMonad<E> some(E value) {
        return new Some<E>(value);
    }

    static <E> MaybeMonad<E> none() {
        return new None<>();
    }

    @SuppressWarnings("RedundantModifiersValueLombok")
    @Value
    @RequiredArgsConstructor(access = AccessLevel.PRIVATE)
    class Some<E> implements MaybeMonad<E> {

        private final E value;

        @Override
        public <F> MaybeMonad<F> map(Function<E, F> mapper) {
            return new Some<>(mapper.apply(value));
        }

        @Override
        public <F> MaybeMonad<F> flatMap(Function<E, MaybeMonad<F>> flatMapper) {
            return flatMapper.apply(value);
        }

        @Override
        public E get() {
            return value;
        }

        @Override
        public boolean isDefined() {
            return true;
        }

    }

    @NoArgsConstructor(access = AccessLevel.PRIVATE)
    class None<E> implements MaybeMonad<E> {

        @Override
        public <F> MaybeMonad<F> map(Function<E, F> mapper) {
            return none();
        }

        @Override
        public <F> MaybeMonad<F> flatMap(Function<E, MaybeMonad<F>> flatMapper) {
            return none();
        }

        @Override
        public E get() {
            throw new UnsupportedOperationException("get");
        }

        @Override
        public boolean isDefined() {
            return false;
        }

    }
}
