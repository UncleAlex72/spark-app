package monads;

import java.util.function.Function;

public interface Monad<E> {

    <F> Monad<F> map(Function<E, F> mapper);

    <F> Monad<F> flatMap(Function<E, Monad<F>> flatMapper);
}
