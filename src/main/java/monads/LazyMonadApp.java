package monads;

public class LazyMonadApp {


    private static EvalMonad<Integer> plus(int i, int j) {
        return EvalMonad.lazy(() -> i + j);
    }

    public static void main(String... args) {

        EvalMonad<Integer> lazy40 =
                EvalMonad.lazy(() -> 1).flatMap(one -> plus(one, 2).flatMap(three -> plus(three, one)).map(four -> four * 10));
        System.out.println("Lazy sum has been created");
        System.out.println(lazy40.get());
    }
}
