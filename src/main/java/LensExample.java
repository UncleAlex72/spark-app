import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.RowFactory;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.types.Metadata;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;
import org.hablapps.sparkOptics.Lens;
import scala.Function1;
import scala.Tuple3;
import scala.runtime.AbstractFunction1;

import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

import static org.apache.spark.sql.types.DataTypes.DoubleType;
import static org.apache.spark.sql.types.DataTypes.StringType;

public class LensExample {

    public static <S, T> Function1<S, T> fn(Function<S, T> f) {
        return new AbstractFunction1<S, T>() {
            @Override
            public T apply(S s) {
                return f.apply(s);
            }
        };
    }
    public static void main(String... args) {
        SparkSession ss =
                SparkSession.builder().appName("Lens").master("local[1]").getOrCreate();
        StructType amountType = new StructType(
                new StructField[] {
                        new StructField("amount", DoubleType, true, Metadata.empty()),
                        new StructField("currency", StringType, true, Metadata.empty())
                }
        );
        StructType debtorType = new StructType(
                new StructField[] {
                        new StructField("name", StringType, true, Metadata.empty()),
                        new StructField("debt", amountType, true, Metadata.empty())
                }
        );
        List<Tuple3<String, Double, String>> rowTemplates = Arrays.asList(
                new Tuple3<>("Freddie", 100.0, "USD"),
                new Tuple3<>("Brian", 50.0, "GBP"),
                new Tuple3<>("Roger", 25.0, "GBP"),
                new Tuple3<>("John", 14.0, "EUR")
        );
        List<Row> rows = rowTemplates.stream().map(template -> {
            String name = template._1();
            Double amount = template._2();
            String currency = template._3();
            return RowFactory.create(name, RowFactory.create(amount, currency));
        }).collect(Collectors.toList());

        Dataset<Row> df = ss.createDataFrame(rows, debtorType);
        Lens lens = Lens.apply("debt.amount", df.schema());
        Dataset<Row> df2 = df.select(lens.modify(fn(column -> column.plus(1))));
        df2.printSchema();
        df2.explain();
        df2.show();
    }
}
