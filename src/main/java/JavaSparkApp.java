import org.apache.spark.Partitioner;
import org.apache.spark.SparkContext;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.broadcast.Broadcast;
import org.apache.spark.sql.SparkSession;
import scala.Tuple2;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class JavaSparkApp {

    public static final List<Integer> INPUT = Stream.iterate(0, i -> i + 1).limit(100).collect(Collectors.toList());

    public static void main(String... args) throws IOException {
        SparkSession spark = SparkSession.builder().appName("Simple Java Application").getOrCreate();
        SparkContext sc = spark.sparkContext();
        try (JavaSparkContext jsc = new JavaSparkContext(sc)) {
            List<String> results = partitionedRdd(jsc).union(broadcastRdd(jsc)).collect();
            try (FileWriter writer = new FileWriter(new File(args[0] + "/out.txt"))) {
                for (String result : results) {
                    writer.write(result);
                    writer.write("\n");
                }
            }
        }
    }

    public static JavaRDD<String> partitionedRdd(JavaSparkContext jsc) {
        List<Integer> input =
                Stream.iterate(0, i -> i + 1).limit(100).collect(Collectors.toList());

        Partitioner partitioner = new Partitioner() {
            @Override
            public int numPartitions() {
                return 5;
            }

            @Override
            public int getPartition(Object key) {
                if (key instanceof Integer) {
                    return (Integer) key;
                } else {
                    return 0;
                }
            }
        };
        return jsc.parallelize(INPUT)
                .mapToPair(i -> new Tuple2<>(i % 5, i))
                .mapPartitions(kvs -> {
                    final long rnd = Math.round(Math.random() * 100);
                    return Iterators.map(kvs, kv ->
                            String.format("Partition: %s: %d: %d", Thread.currentThread().getName(), rnd, kv._2()));

                });
    }

    public static JavaRDD<String> broadcastRdd(JavaSparkContext jsc) {
        Broadcast<Integer> broadcast = jsc.broadcast(10);
        return jsc.parallelize(INPUT).map(value ->
                String.format("Broadcast: %s: %d: %d", Thread.currentThread().getName(), broadcast.getValue(), value));
    }
}
