package map;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.spark.sql.*;
import static org.apache.spark.sql.functions.*;
import org.apache.spark.sql.catalyst.encoders.RowEncoder;

import java.io.Serializable;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class NativeMapEncoding {

    public static void main(String... args) {
        try (SparkSession session = SparkSession.builder().appName("map").master("local[*]").getOrCreate()) {
            Map<Numero, Numero> map = new HashMap<>();
            map.put(Numero.ONE, Numero.TWO);
            map.put(Numero.THREE, Numero.FOUR);
            Wrapper wrapper =
                    Wrapper.builder()
                            .map(map)
                            .build();
            Encoder<Wrapper> encoder = Encoders.bean(Wrapper.class);
            Dataset<Row> ds =
                session.createDataset(Collections.singletonList(wrapper), encoder).as(RowEncoder.apply(encoder.schema()));
            ds.printSchema();
            ds.show();
            Dataset<Row> mds =
                    ds.select(explode(col("map"))).select(col("key").as("k"), col("value").as("v"));
            mds.printSchema();
            mds.show();
        }
    }

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Wrapper implements Serializable {

        private Map<Numero, Numero> map;
    }

}
