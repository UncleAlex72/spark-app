package map;

import lombok.*;
import org.apache.spark.sql.*;
import org.apache.spark.sql.catalyst.encoders.RowEncoder;

import java.io.Serializable;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.apache.spark.sql.functions.col;
import static org.apache.spark.sql.functions.explode;

public class MapEncoding {

    public static void main(String... args) {
        try (SparkSession session = SparkSession.builder().appName("map").master("local[*]").getOrCreate()) {
            KvListMap<Numero, Numero> map = new KvListMap<>();
            map.put(Numero.ONE, Numero.TWO);
            map.put(Numero.THREE, Numero.FOUR);
            Wrapper wrapper =
                    Wrapper.builder()
                            .map(map)
                            .build();
            Encoder<Wrapper> encoder = Encoders.bean(Wrapper.class);
            Dataset<Row> ds =
                session.createDataset(Collections.singletonList(wrapper), encoder).as(RowEncoder.apply(encoder.schema()));
            ds.printSchema();
            ds.show();
            Dataset<Row> arr = ds.select(explode(col("map.entries")).as("arr"));
            arr.printSchema();
            arr.show();

            ds.as(encoder).collectAsList().stream().map(Wrapper::getMap).flatMap(m -> m.entrySet().stream())
                    .forEach(System.out::println);
        }
    }

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Wrapper implements Serializable {

        private KvListMap<Numero, Numero> map;
    }

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    public static class KV<K extends Serializable, V extends Serializable> implements Map.Entry<K, V>,  Serializable {
        private K k;
        private V v;

        public V setValue(V newValue) {
            V oldValue = v;
            v = newValue;
            return oldValue;
        }

        @Override
        public K getKey() {
            return k;
        }

        @Override
        public V getValue() {
            return v;
        }
    }

    // This *must not* implement map!
    @AllArgsConstructor
    @Getter
    @Setter
    @EqualsAndHashCode
    @ToString
    public static abstract class ListMap<K extends Serializable, V extends Serializable, E extends Map.Entry<K, V>> implements Serializable {

        private List<E> entries;

        protected abstract E createEntry(K key, V value);

        private Stream<E> entryStream() {
            return entries.stream();
        }

        public int size() {
            return entries.size();
        }

        public boolean isEmpty() {
            return entries.isEmpty();
        }

        public boolean containsKey(Object key) {
            return entryStream().map(Map.Entry::getKey).anyMatch(k -> k.equals(key));
        }

        public boolean containsValue(Object value) {
            return entryStream().map(Map.Entry::getValue).anyMatch(v -> v.equals(value));
        }

        public V get(Object key) {
            return entryStream().filter(entry -> entry.getKey().equals(key)).findFirst().map(Map.Entry::getValue).orElse(null);
        }

        public V put(K key, V value) {
            V previousResult = remove(key);
            entries.add(createEntry(key, value));
            return previousResult;
        }

        public V remove(Object key) {
            Optional<E> maybeExistingValue = entryStream().filter(entry -> entry.getKey().equals(key)).findFirst();
            maybeExistingValue.ifPresent(entry -> entries.remove(entry));
            return maybeExistingValue.map(Map.Entry::getValue).orElse(null);
        }

        public void putAll(Map<? extends K, ? extends V> m) {
            m.forEach(this::put);
        }

        public void clear() {
            entries.clear();
        }

        public Set<K> keySet() {
            return entryStream().map(Map.Entry::getKey).collect(Collectors.toSet());
        }

        public Collection<V> values() {
            return entryStream().map(Map.Entry::getValue).collect(Collectors.toList());
        }

        public Set<Map.Entry<K, V>> entrySet() {
            return entryStream().collect(Collectors.toSet());
        }

        public Map<K, V> asMap() {
            return new Map<K, V>() {

                @Override
                public int size() {
                    return ListMap.this.size();
                }

                @Override
                public boolean isEmpty() {
                    return ListMap.this.isEmpty();
                }

                @Override
                public boolean containsKey(Object key) {
                    return ListMap.this.containsKey(key);
                }

                @Override
                public boolean containsValue(Object value) {
                    return ListMap.this.containsValue(value);
                }

                @Override
                public V get(Object key) {
                    return ListMap.this.get(key);
                }

                @Override
                public V put(K key, V value) {
                    return ListMap.this.put(key, value);
                }

                @Override
                public V remove(Object key) {
                    return ListMap.this.remove(key);
                }

                @Override
                public void putAll(Map<? extends K, ? extends V> m) {
                    ListMap.this.putAll(m);
                }

                @Override
                public void clear() {
                    ListMap.this.clear();
                }

                @Override
                public Set<K> keySet() {
                    return ListMap.this.keySet();
                }

                @Override
                public Collection<V> values() {
                    return ListMap.this.values();
                }

                @Override
                public Set<Entry<K, V>> entrySet() {
                    return ListMap.this.entrySet();
                }
            };
        }
    }

    @Data
    @Builder
    @EqualsAndHashCode(callSuper = true)
    @ToString(callSuper = true)
    public static class KvListMap<K extends Serializable, V extends Serializable> extends ListMap<K, V, KV<K, V>> {

        public KvListMap() {
            super(new ArrayList<>());
        }

        @Override
        protected KV<K, V> createEntry(K key, V value) {
            return KV.<K, V>builder().k(key).v(value).build();
        }
    }

}
