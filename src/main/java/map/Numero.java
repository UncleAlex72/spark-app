package map;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
public class Numero implements Serializable {

    public static Numero ONE = Numero.builder().english("one").greek("ένα").build();
    public static Numero TWO = Numero.builder().english("two").greek("δύο").build();
    public static Numero THREE = Numero.builder().english("three").greek("τρία").build();
    public static Numero FOUR = Numero.builder().english("four").greek("τέσσερα").build();

    private String english;
    private String greek;
}
