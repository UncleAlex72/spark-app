package models;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.List;

public final class Outer {

    private final String name;
    private final int age;
    private final List<Inner> inners;

    public Outer(String name, int age, List<Inner> inners) {
        this.name = name;
        this.age = age;
        this.inners = inners;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public boolean equals(Object o) {
        return EqualsBuilder.reflectionEquals(this, o, false);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this, false);
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public List<Inner> getInners() {
        return inners;
    }
}
