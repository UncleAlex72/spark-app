package models;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.List;
import java.util.stream.Collectors;

public final class OuterBean {

    public static OuterBean fromOuter(Outer outer) {
        String name = outer.getName();
        int age = outer.getAge();
        List<InnerBean> innerBeans = outer.getInners().stream().map(InnerBean::fromInner).collect(Collectors.toList());
        return new OuterBean(name, age, innerBeans);
    }

    private String name;
    private int age;
    private List<InnerBean> inners;

    public OuterBean() {

    }

    public OuterBean(String name, int age, List<InnerBean> inners) {
        this.name = name;
        this.age = age;
        this.inners = inners;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public boolean equals(Object o) {
        return EqualsBuilder.reflectionEquals(this, o, false);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this, false);
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setInners(List<InnerBean> inners) {
        this.inners = inners;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public List<InnerBean> getInners() {
        return inners;
    }
}
