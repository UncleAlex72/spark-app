package models;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

public final class InnerBean {

    public static InnerBean fromInner(Inner inner) {
        String name = inner.getName();
        int age = inner.getAge();
        return new InnerBean(name, age);
    }


    private String name;
    private int age;

    public InnerBean() {
    }

    public InnerBean(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public Inner toInner() {
        return new Inner(name, age);
    };

    @Override
    public boolean equals(Object o) {
        return EqualsBuilder.reflectionEquals(this, o, false);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this, false);
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }
}
