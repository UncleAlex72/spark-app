import java.util.Iterator;
import java.util.function.Function;

public final class Iterators {

    public static <T, U> Iterator<U> map(final Iterator<T> ts, Function<T, U> mapper) {
        return new Iterator<U>() {
            @Override
            public boolean hasNext() {
                return ts.hasNext();
            }

            @Override
            public void remove() {
                ts.remove();
            }

            @Override
            public U next() {
                return mapper.apply(ts.next());
            }
        };
    }
}
