package parquet;

import io.vavr.Predicates;
import io.vavr.collection.*;
import io.vavr.control.Option;
import lombok.SneakyThrows;
import org.apache.avro.data.TimeConversions;
import org.apache.avro.generic.GenericData;
import org.apache.avro.generic.GenericRecord;
import org.apache.avro.util.Utf8;
import org.apache.commons.io.FileUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.parquet.avro.AvroParquetReader;
import org.apache.parquet.hadoop.ParquetReader;
import org.apache.parquet.hadoop.util.HadoopInputFile;
import org.apache.parquet.io.InputFile;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.RowFactory;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.types.Metadata;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;

import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.function.Supplier;

import static org.apache.spark.sql.types.DataTypes.*;

public class MyParquetReader {

  public static void main(String... args) throws Exception {
    Path parquetDir = Paths.get("target/parquet-out");
    if (args.length > 0) {
      writeParquetFile(parquetDir);
    }
    new MyParquetReader().readDirectory(parquetDir).forEach(System.out::println);
  }

  public Seq<Map<String, Option<Object>>> readDirectory(Path directory) throws IOException {
    List<Path> files = new ArrayList<>();
    try (DirectoryStream<Path> directoryStream = Files.newDirectoryStream(directory)) {
      directoryStream.forEach(files::add);
    }
    return Vector.ofAll(files)
        .filter(path -> path.getFileName().toString().endsWith(".parquet"))
        .flatMap(this::streamFile);
  }

  @SneakyThrows
  public Stream<Map<String, Option<Object>>> streamFile(Path file) {
    InputFile inputFile =
        HadoopInputFile.fromPath(
            new org.apache.hadoop.fs.Path(file.toString()), new Configuration());
    GenericData genericData = new GenericData();
    Array.of(new TimeConversions.DateConversion(), new TimeConversions.TimestampMicrosConversion())
        .forEach(genericData::addLogicalTypeConversion);

    try (ParquetReader<GenericRecord> reader =
        AvroParquetReader.<GenericRecord>builder(inputFile).withDataModel(genericData).build()) {
      //noinspection Convert2Lambda
      return Stream.iterate(
              new Supplier<Option<GenericRecord>>() {
                @Override
                @SneakyThrows
                public Option<GenericRecord> get() {
                  return Option.of(reader.read());
                }
              })
          .takeWhile(Predicates.isNotNull())
          .map(this::parseRecord);
    }
  }

  private Map<String, Option<Object>> parseRecord(GenericRecord record) {
    Map<String, Option<Object>> empty = HashMap.empty();
    return io.vavr.collection.List.ofAll(record.getSchema().getFields())
        .foldLeft(
            empty,
            (map, field) -> {
              String name = field.name();
              Object rawValue = record.get(name);
              Object convertedValue = parse(rawValue);
              return map.put(name, Option.of(convertedValue));
            });
  }

  private Object parse(Object o) {
    if (o instanceof GenericRecord) {
      return parseRecord((GenericRecord) o);
    } else if (o instanceof List) {
      //noinspection unchecked
      return Vector.ofAll((List<GenericRecord>) o).map(r -> r.get("element")).map(this::parse);
    } else if (o instanceof Utf8) {
      return ((Utf8) o).toString();
    } else if (o == null) {
      return null;
    } else if (o.getClass().getPackage().getName().startsWith("java.")) {
      return o;
    } else {
      throw new IllegalStateException("Don't know what to do!!");
    }
  }

  public static void writeParquetFile(Path parquetDir) throws IOException {
    FileUtils.deleteDirectory(parquetDir.toFile());
    try (SparkSession session =
        SparkSession.builder()
            .master("local[*]")
            .appName("parquet-out")
            .config("spark.sql.parquet.outputTimestampType", "TIMESTAMP_MICROS")
            .config("spark.sql.datetime.java8API.enabled", "true")
            .getOrCreate()) {
      StructType artist =
          new StructType(
              new StructField[] {
                new StructField("name", StringType, true, Metadata.empty()),
                new StructField("instruments", createArrayType(StringType), true, Metadata.empty()),
                new StructField("number", LongType, true, Metadata.empty())
              });
      StructType ref =
          new StructType(
              new StructField[] {
                new StructField("id1", StringType, true, Metadata.empty()),
                new StructField("id2", StringType, true, Metadata.empty())
              });
      StructType schema =
          new StructType(
              new StructField[] {
                new StructField("name", StringType, true, Metadata.empty()),
                new StructField("artists", createArrayType(artist), true, Metadata.empty()),
                new StructField("ref", ref, true, Metadata.empty()),
                new StructField("good", BooleanType, true, Metadata.empty()),
                new StructField("when", TimestampType, true, Metadata.empty())
              });
      Row freddie = RowFactory.create("freddie", new String[] {"vocals", "piano"}, 1L);
      Row brian = RowFactory.create("brian", new String[] {"guitar"}, 2L);
      Row queen =
          RowFactory.create(
              "Queen",
              new Row[] {freddie, brian},
              RowFactory.create("a", null),
              true,
              Instant.now());
      Dataset<Row> ds = session.createDataFrame(Collections.singletonList(queen), schema);
      ds.write().parquet(parquetDir.toString());
    }
  }
}
