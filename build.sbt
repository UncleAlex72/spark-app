name := "Spark App"

version := "1.0"

scalaVersion := "2.12.10"
javacOptions ++= Seq("-source", "8", "-target", "8")
libraryDependencies += "org.apache.spark" %% "spark-sql" % "3.0.0"
libraryDependencies += "org.hablapps" %% "spark-optics" % "0.1.1"
libraryDependencies += "org.projectlombok" % "lombok" % "1.16.16"
libraryDependencies += "com.google.guava" % "guava" % "30.0-jre"
libraryDependencies += "org.apache.parquet" % "parquet-avro" % "1.11.1"
libraryDependencies += "io.vavr" % "vavr" % "0.10.3"
